# YouLess Home Assistant Component

This component adds support for the [Youless](http://www.youless.nl) energy meter reader to Home Assistant. The component will
create a sensor for the current power consumption measured.

Please keep in mind this repository is only used to test new features before they are ready to be integrated into the version
included with Home Assistant. So this version will be bleeding edge, and the version maintained in Home Assistant will 
be stable.

**Note:** Support for the LS110 is experimental currently and is registered in issue [YA-1](https://jongsoftdev.atlassian.net/browse/YA-1).

**Note 2:** Please note that only the default firmware is supported at this time, support for pvoutput is under investigation in 
[YA-11](https://jongsoftdev.atlassian.net/browse/YA-11)

**Warning:** The current version has breaking changes in the names of sensors, this will break integrations and lovelace usage.

## Installation instructions

* Download one of the releases of this component
* Upload the component in the following directory on home assistant

```
/config
```

## Configuration

[![My-Home-Assistant](https://my.home-assistant.io/badges/config_flow_start.svg)](https://my.home-assistant.io/redirect/config_flow_start?domain=youless)

Adding the YouLess integration to your Home Assistant installation is easy, by following these steps:

* Browse to your Home Assistant instance.
* In the sidebar click on ![Cog](.supporting/cog.png) [Configuration](https://my.home-assistant.io/redirect/config).
* From the configuration menu select: ![Integration](.supporting/puzzle.png) [Integrations](https://my.home-assistant.io/redirect/integrations).
* In the bottom right, click on the ![Add](.supporting/plus-circle.png) [Add Integration](https://my.home-assistant.io/redirect/config_flow_start?domain=youless) button.
* From the list, search and select ***“Youless”***.
* Follow the instruction on screen to complete the set up.

You will need the following two things when creating the integration:

* the host, this should be the IP address of the device

The integration will automatically create a device and various sensors in your Home Assistant. 
The integration will attempt to determine if you have a LS110 or a LS120 device.

## Exposed sensors

Which sensors you get depends on which device you have. The integration should create one device with the following sensors:

* Current power consumption
* Total power consumption

Additionally for the LS120 only:

* Total gas consumption
* Power consumption low
* Power consumption high
* Power delivery high
* Power delivery low
* Extra current
* Extra total
